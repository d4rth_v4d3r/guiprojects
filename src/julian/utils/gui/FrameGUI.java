package julian.utils.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import julian.utils.gui.EditorCodigo.StatusFlag;

public class FrameGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 30011587946355314L;
	private JPanel contentPane;
	private JMenuBar menuBar;

	public static void main(String[] args) {
		char[] heap = new char[300];
		int h = 0, p = 0;

		Double n = new Double("12345678.3244");
		Double i = new Double("1");

		while (n / i >= 10)
			i *= 10;

		while (i >= 1) {
			heap[h] = (char) ((n / i) + 48);
			n = n % i;
			i /= 10;
			h = h + 1;
		}

		if (n != 0) {
			heap[h] = '.';
			h = h + 1;
			int j = 0;
			while (j < 6) {
				heap[h] = (char) ((n / i) + 48);
				n = n % i;
				i /= 10;
				j++;
				h = h + 1;
			}
		}

		System.out.println(Arrays.toString(heap));
		System.out.println((int) '.');
	}

	/**
	 * Launch the application.
	 */
	public static void mainf(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					try {
						UIManager
								.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					} catch (Exception e) {// Manejo de excepción...
						try {
							UIManager
									.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
						} catch (Exception e1) {// Manejo de excepción...
							e.printStackTrace();
						}
					}
					final FrameGUI frame = new FrameGUI();
					final EditorGUI editor = new EditorGUI();
					String[] archivo = new String[] { "Abrir", "Salir" };
					String[] ejecutar = new String[] { "Ejecutar" };

					editor.agregarComponente("MySQL.db", new EditorCodigo(),
							-1, EditorGUI.SALIDAS, true);

					ActionListener[] archivo_listeners = new ActionListener[] {
							new ActionListener() {

								@Override
								public void actionPerformed(ActionEvent e) {
									// TODO Auto-generated method stub
									EditorCodigo ide = new EditorCodigo();
									File f = frame.abrirArchivo(
											"Archivos de 3 direcciones.",
											".3d", ide);
									if (f != null) {
										editor.agregarComponente(f.getName(),
												ide, -1, EditorGUI.ENTRADAS,
												false);
										ide.cargarArchivo(f);
										ide.editable(false);
									}
								}
							}, new ActionListener() {

								@Override
								public void actionPerformed(ActionEvent e) {
									// TODO Auto-generated method stub
									System.exit(0);
								}
							} };

					frame.agregarMenu("Archivo", archivo, archivo_listeners);
					frame.setContentPane(editor);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 617, 440);

		this.menuBar = new JMenuBar();
		setJMenuBar(this.menuBar);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(this.contentPane);
	}

	/**
	 * Agrega un menu en la barra de herramientas.
	 * 
	 * @param nombre
	 *            El nombre del menu.
	 * @param items
	 *            La lista de items del menu.
	 * @param listeners
	 *            Las acciones que ejecutará cada item.
	 */
	public void agregarMenu(String nombre, String[] items,
			ActionListener[] listeners) {
		if (items.length != listeners.length) {
			System.err
					.println("El tamaño de la lista de items y listeners debe ser el mismo");
			return;
		}
		JMenu jm = new JMenu(nombre);
		int index = 0;
		for (String s : items) {
			JMenuItem jmi = new JMenuItem(s);
			jmi.addActionListener(listeners[index]);
			++index;
			jm.add(jmi);
		}

		this.menuBar.add(jm);
	}

	/**
	 * Abre un archivo en un editor.
	 * 
	 * @param desc_filtro
	 *            La descripcion del filtro
	 * @param filtro
	 *            La extension del filtro
	 * @param editor
	 *            El editor donde se va a cargar
	 * @return El archivo cargado
	 */
	public File abrirArchivo(String desc_filtro, String filtro,
			EditorCodigo editor) {
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				desc_filtro, filtro);
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(filter);
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		} else if (returnVal == JFileChooser.ERROR_OPTION)
			editor.actualizarEstado("Error al abrir el archivo.",
					StatusFlag.ERROR);
		else
			editor.actualizarEstado("Error desconocido.", StatusFlag.ERROR);
		return null;
	}
}
