package julian.utils.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class EditorCodigo extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7179485580848595548L;
	private JTextArea editor;
	private JTextArea line_num;
	private int line_count;
	private JPanel contenedor;
	private JLabel barraEstado;
	private String filename;
	private File f;

	public enum StatusFlag {
		ERROR, OK;
	};

	/**
	 * Create the panel.
	 */
	public EditorCodigo() {
		setLayout(new BorderLayout(0, 0));
		Font font = new Font("Consolas", Font.PLAIN, 14);
		++line_count;

		this.contenedor = new JPanel();
		add(this.contenedor, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();

		editor = new JTextArea();
		scrollPane.setViewportView(editor);
		editor.setFont(font);
		editor.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub
				try {
					String newText = arg0.getDocument().getText(0,
							arg0.getDocument().getLength());
					int lines = StringUtils.countMatches(newText, "\n");
					line_count = 0;
					line_num.setText("");
					for (int i = 0; i <= lines; i++)
						line_num.getDocument().insertString(
								line_num.getDocument().getLength(),
								++line_count + ".\n", null);
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub
				try {
					String newText = arg0.getDocument().getText(
							arg0.getOffset(), arg0.getLength());
					for (int i = 0; i < StringUtils.countMatches(newText, "\n"); i++)
						line_num.getDocument().insertString(
								line_num.getDocument().getLength(),
								++line_count + ".\n", null);
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		line_num = new JTextArea();
		this.line_num.setBackground(Color.DARK_GRAY);
		line_num.setEditable(false);
		line_num.setEnabled(false);
		line_num.setText("1.\n");
		scrollPane.setRowHeaderView(line_num);
		line_num.setFont(font);

		this.contenedor.setLayout(new BorderLayout(0, 0));
		this.contenedor.add(scrollPane);
		this.barraEstado = new JLabel("Archivo nuevo :)");
		this.barraEstado.setFont(new Font("Consolas", Font.BOLD, 14));
		this.barraEstado.setHorizontalAlignment(SwingConstants.CENTER);
		add(this.barraEstado, BorderLayout.SOUTH);
	}

	public void cargarArchivo(String ruta) {
		this.filename = ruta;
		cargarArchivo(new File(ruta));
	}

	public void cargarArchivo(File archivo) {
		this.filename = archivo.getAbsolutePath();
		String content = "";
		try {
			this.f = archivo;
			content = FileUtils.readFileToString(archivo,
					Charset.defaultCharset());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			this.actualizarEstado(
					"Error de lectura en el archivo "
							+ archivo.getAbsolutePath(), StatusFlag.ERROR);
		}

		this.setTexto(content.toString());
		this.actualizarEstado("Archivo " + archivo.getAbsolutePath()
				+ " abierto.", StatusFlag.OK);
	}

	public void guardarArchivo(String ruta) {
		this.filename = ruta;
		guardarArchivo(new File(ruta));
	}

	public void guardarArchivo(File archivo) {
		this.filename = archivo.getAbsolutePath();
		try {
			this.f = archivo;
			FileUtils.write(archivo, this.getTexto(), Charset.defaultCharset());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			this.actualizarEstado(
					"Error de escritura en el archivo "
							+ archivo.getAbsolutePath(), StatusFlag.ERROR);
		}

		this.actualizarEstado("Archivo " + archivo.getAbsolutePath()
				+ " guardado.", StatusFlag.OK);
	}

	public void editable(boolean si_no) {
		this.editor.setEditable(si_no);
	}

	public void setTexto(String content) {
		this.editor.setText(content);
	}

	public String getTexto() {
		return this.editor.getText();
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public File getF() {
		return f;
	}

	public void setF(File f) {
		this.f = f;
	}

	public void refrescarArchivo() {
		this.cargarArchivo(this.filename);
	}

	public void actualizarEstado(String status, StatusFlag flag) {
		if (flag == StatusFlag.ERROR)
			this.barraEstado.setForeground(Color.RED);
		else
			this.barraEstado.setForeground(Color.BLACK);
		this.barraEstado.setText(status);
	}

}
