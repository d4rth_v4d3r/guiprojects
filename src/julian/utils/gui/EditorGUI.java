package julian.utils.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class EditorGUI extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3950986176786775505L;
	private JTabbedPane salidas;
	private JTabbedPane entradas;
	public static final int SALIDAS = 0, ENTRADAS = 1;

	/**
	 * Create the panel.
	 */
	public EditorGUI() {

		this.salidas = new JTabbedPane(JTabbedPane.TOP);
		this.salidas.setAlignmentX(Component.RIGHT_ALIGNMENT);
		this.salidas.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

		this.entradas = new JTabbedPane(JTabbedPane.TOP);
		this.entradas.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		this.entradas.setAlignmentX(Component.LEFT_ALIGNMENT);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addComponent(this.entradas, GroupLayout.DEFAULT_SIZE,
								308, Short.MAX_VALUE)
						.addGap(55)
						.addComponent(this.salidas, GroupLayout.DEFAULT_SIZE,
								321, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout
				.createParallelGroup(Alignment.LEADING)
				.addComponent(this.entradas, GroupLayout.DEFAULT_SIZE, 460,
						Short.MAX_VALUE)
				.addComponent(this.salidas, Alignment.TRAILING,
						GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE));
		setLayout(groupLayout);
	}

	/**
	 * Agrega un componente en una nueva pestaña en los contenedores de
	 * pestañas.
	 * 
	 * @param titulo
	 *            El titulo de la pestaña.
	 * @param contenido
	 *            El componente que irá dentro de la pestaña.
	 * @param posicion
	 *            La posición de la pestaña en el grupo.
	 * @param contenedor
	 *            Si el contenedor es de entradas o salidas. ENTRADAS/SALIDAS.
	 * 
	 * @param fijo
	 *            Si la pestaña no es fija, se agrega un botón de cerrar la
	 *            pestaña.
	 */
	public void agregarComponente(String titulo, Component contenido,
			int posicion, int contenedor, boolean fijo) {
		JTabbedPane jtp = contenedor == SALIDAS ? this.salidas
				: contenedor == ENTRADAS ? this.entradas : null;
		try {
			if (posicion >= 0) {
				try {

					Dimension d = jtp.getSize();
					Dimension d1 = entradas.getMinimumSize();
					Dimension d2 = salidas.getMinimumSize();

					entradas.setMinimumSize(new Dimension(d));
					salidas.setMinimumSize(new Dimension(d));

					jtp.add(contenido, posicion);
					if (fijo)
						jtp.setTitleAt(posicion, titulo);
					else
						jtp.setTabComponentAt(posicion,
								crearBotonCerrar(titulo, contenido, jtp));
					contenido.setSize(jtp.getSize());

					entradas.setMinimumSize(d1);
					salidas.setMinimumSize(d2);
				} catch (IndexOutOfBoundsException e) {
					System.err.println("La posicion " + posicion
							+ " esta fuera de los limites.");
				}
			} else {
				Dimension d = jtp.getSize();
				Dimension d1 = entradas.getMinimumSize();
				Dimension d2 = salidas.getMinimumSize();

				entradas.setMinimumSize(new Dimension(d));
				salidas.setMinimumSize(new Dimension(d));

				jtp.add(contenido);
				if (jtp == this.salidas)
					this.entradas.setSize(jtp.getSize());
				else
					this.salidas.setSize(jtp.getSize());
				if (fijo)
					jtp.setTitleAt(jtp.indexOfComponent(contenido), titulo);
				else
					jtp.setTabComponentAt(jtp.indexOfComponent(contenido),
							crearBotonCerrar(titulo, contenido, jtp));

				entradas.setMinimumSize(d1);
				salidas.setMinimumSize(d2);
			}
		} catch (NullPointerException e) {
			System.err
					.println("No definiste un contenedor existente. ENTRADAS/SALIDAS ");
		}

	}

	/**
	 * @param contenedor
	 *            El contenedor, de ENTRADAS o SALIDAS
	 * @return El componente actualmente desplegado.
	 */
	public Component componenteActual(int contenedor) {
		JTabbedPane jtp = contenedor == SALIDAS ? this.salidas
				: contenedor == ENTRADAS ? this.entradas : null;
		try {
			if (jtp.getSelectedIndex() == -1)
				return null;
			return jtp.getComponentAt(jtp.getSelectedIndex());
		} catch (NullPointerException e) {
			System.err
					.println("No definiste un contenedor existente. ENTRADAS/SALIDAS ");
			return null;
		}
	}

	/**
	 * Cambia el titulo a la pestana que contiene al componente tab
	 * 
	 * @param contenedor
	 *            El contenedor ENTRADAS/SALDIAS
	 * @param tab
	 *            El componente que se desea cambiar el titulo
	 * @param title
	 *            El nuevo titulo
	 */
	public void cambiarNombreComponente(int contenedor, Component tab,
			String title) {
		JTabbedPane jtp = contenedor == SALIDAS ? this.salidas
				: contenedor == ENTRADAS ? this.entradas : null;
		try {
			int index = -1;
			for (int i = 0; i < jtp.getTabCount(); i++)
				if (jtp.getComponentAt(i) == tab) {
					index = i;
					break;
				}

			Component c = jtp.getTabComponentAt(index);
			if (c instanceof JTitulo)
				((JTitulo) c).title.setText(title);
			jtp.setTitleAt(index, title);
		} catch (NullPointerException e) {
			System.err
					.println("No definiste un contenedor existente. ENTRADAS/SALIDAS ");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err
					.println("No definiste un contenedor existente. ENTRADAS/SALIDAS ");
		}
	}

	private JPanel crearBotonCerrar(String titulo, final Component contenido,
			final JTabbedPane contenedor) {
		return new JTitulo(titulo, contenido, contenedor);
	}

	class JTitulo extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = -4913349871253295685L;
		JButton closeButton;
		JLabel title;

		JTitulo(String titulo, final Component contenido,
				final JTabbedPane contenedor) {
			this.setLayout(new FlowLayout());

			JButton jb = new JButton() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -4503610053974696405L;

				@Override
				public void paint(Graphics g) {
					// TODO Auto-generated method stub
					super.paintComponent(g);
					Graphics2D g2 = (Graphics2D) g.create();
					// shift the image for pressed buttons
					if (getModel().isPressed()) {
						g2.translate(1, 1);
					}
					g2.setStroke(new BasicStroke(2));
					g2.setColor(Color.BLACK);
					if (getModel().isRollover()) {
						g2.setColor(Color.MAGENTA);
					}
					int delta = 6;
					g2.drawLine(delta, delta, getWidth() - delta - 1,
							getHeight() - delta - 1);
					g2.drawLine(getWidth() - delta - 1, delta, delta,
							getHeight() - delta - 1);
					g2.dispose();
				}
			};
			jb.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					contenedor.remove(contenedor.indexOfComponent(contenido));
				}
			});
			jb.setFocusable(false);
			jb.setContentAreaFilled(false);
			jb.setPreferredSize(new Dimension(17, 17));
			jb.setOpaque(false);
			jb.setToolTipText("Cerrar esta pestaña.");

			JLabel jl = new JLabel(titulo);
			this.add(jl);
			this.add(jb);
			this.setOpaque(false);

			this.closeButton = jb;
			this.title = jl;
		}
	}
}
